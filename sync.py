#!/usr/bin/python3

import json
import os
import shutil
import subprocess
import traceback
from fdroidserver import common
from fdroidserver import index

url = 'https://f-droid.org/repo'
fingerprint = '43238D512C1E5EB2D6569F4A3AFBF5523418B82E0A3ED1552770ABB9A9C9CCAB'
timeout = 30
retryCount = 3

common.config = { 'jarsigner': 'jarsigner' }

etag = ''
try:
    with open('etag', 'r') as file:
        etag = file.read().splitlines()[0]
except:
    pass

url = url + '?fingerprint=' + fingerprint
while True:
    try:
        indexdata, etag = index.download_repo_index(url, etag = etag, timeout = timeout)
        break
    except KeyboardInterrupt:
        raise
    except:
        if retryCount > 0:
            retryCount = retryCount - 1
            traceback.print_exc()
        else:
            raise

applications = None
packages = None
try:
    if indexdata != None:
        applications = indexdata['apps']
        packages = indexdata['packages']
except:
    pass

if applications != None and type(applications) is list and \
    packages != None and type(packages) is dict:
    if os.path.isdir('applications'):
        shutil.rmtree('applications')
    os.mkdir('applications')

    for application in applications:
        packageName = None
        suggestedVersionCode = None
        try:
            packageName = application['packageName']
            try:
                suggestedVersionCode = int(application['suggestedVersionCode'])
            except:
                pass
        except:
            traceback.print_exc()

        if packageName != None:
            currentPackages = []
            try:
                currentPackages = packages[packageName]
                if not type(currentPackages) is list:
                    currentPackages = []
            except:
                traceback.print_exc()

            resultPackages = []
            for package in currentPackages:
                try:
                    versionName = package['versionName']
                    versionCode = package['versionCode']
                    resultPackages.append({
                        'versionName': versionName,
                        'versionCode': versionCode
                    })
                except Exception as e:
                    traceback.print_exc()

            if len(resultPackages) > 0:
                data = {
                    'packageName': packageName,
                    'suggestedVersionCode': suggestedVersionCode,
                    'packages': resultPackages
                }
                with open('applications/' + packageName, 'w') as file:
                    json.dump(data, file)

    if etag != None and len(etag) > 0:
        with open('etag', 'w') as file:
            file.write(etag)

    subprocess.check_call(["git", "add", "applications", "etag"])
    subprocess.call(["git", "commit", "-m", "Update applications"])
