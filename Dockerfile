FROM debian:buster-slim
RUN mkdir -p /usr/share/man/man1 && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
    openssh-client \
    locales \
    python3 \
    git \
    curl \
    ca-certificates \
    python3-git \
    python3-pyasn1 \
    python3-pyasn1-modules \
    python3-yaml \
    python3-ruamel.yaml \
    python3-requests \
    python3-setuptools \
    default-jdk-headless && \
    apt-get clean && \
    mkdir fdroidserver && \
    cd fdroidserver && \
    curl -s https://gitlab.com/fdroid/fdroidserver/repository/master/archive.tar.gz | \
    tar -xz --strip-components=1 && \
    ./setup.py compile_catalog && \
    ./setup.py bdist_egg && \
    ./setup.py install && \
    cd .. && \
    rm -rf fdroidserver
RUN echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen && locale-gen
ENV LANG en_US.UTF-8 
